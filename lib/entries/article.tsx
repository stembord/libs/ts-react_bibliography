import * as React from "react";
import { parseAuthor, parseMonth } from "../utils";

export const ARTICLE_PROPS_KEYS = [
  "author",
  "title",
  "journal",
  "year",
  "volume",
  "number",
  "pages",
  "month",
  "doi",
  "note"
];

interface IArticleProps {
  author: string;
  title: string;
  journal: string;
  year: string;
  volume?: string;
  number?: string;
  pages?: string;
  month?: string;
  note?: string;
  doi?: string;
  url?: string;
}

interface IArticleState {}

export class Article extends React.Component<IArticleProps, IArticleState> {
  render() {
    const { author, title, journal, year } = this.props;

    const parsedAuthor = parseAuthor(author);

    return (
      <div className="BibItem">
        <span className="BibItem--author">{parsedAuthor}, </span>
        {this.title(title)}
        <span className="BibItem--journal">
          <i> {journal}</i>,{" "}
        </span>
        {this.volume(this.props.volume)}
        {this.pages(this.props.pages)}
        {this.month(this.props.month)}
        <span className="BibItem--year">{year}. </span>
        {this.props.note ? (
          <span className="BibItem-note">{this.props.note}. </span>
        ) : null}
        {this.doi(this.props.doi, this.props.url)}
      </div>
    );
  }

  title(title: string) {
    return <span className="BibItem--title">{`“${title},” `}</span>;
  }

  volume(volume: string | undefined) {
    if (volume) {
      return <span className="BibItem--volume">{`vol. ${volume}, `}</span>;
    } else {
      return null;
    }
  }

  pages(pages: string | undefined) {
    if (!pages) {
      return null;
    } else {
      return <span className="BibItem--pages">{`pp. ${pages}, `}</span>;
    }
  }

  month(month: string | undefined) {
    if (!month) {
      return null;
    } else {
      const parsedMonth = parseMonth(month);
      return <span className="BibItem--month">{parsedMonth} </span>;
    }
  }

  doi(doi: string | undefined, url: string | undefined) {
    if (!doi) {
      return null;
    } else {
      return (
        <span className="BibItem--doi">
          <a href={url ? url : "#"}>DOI: {doi}</a>
        </span>
      );
    }
  }
}
