import * as React from "react";
import { Article, ARTICLE_PROPS_KEYS } from "./entries/article";

interface IBibItemProps {
  type: string;
  author?: string;
  title?: string;
  journal?: string;
  year?: string;
  volume?: string;
  number?: string;
  pages?: string;
  month?: string;
  note?: string;
}

interface IBibItemState {}

export class BibItem extends React.Component<IBibItemProps, IBibItemState> {
  render() {
    const type = this.props.type.toLocaleLowerCase();

    switch (type) {
      case "article":
        return <Article {...this.props} />;
      default:
        throw new Error(`invalid type: ${type}`);
    }
  }
}

interface ILinkedBibItemProps extends IBibItemProps {
  headline: string;
  link: string;
  linkLabel: string;
}

interface ILinkedBibItemState {}

export class LinkedBibItem extends React.Component<
  ILinkedBibItemProps,
  ILinkedBibItemState
> {
  render() {
    const { link, linkLabel, headline, ...rest } = this.props;

    return (
      <div className="LinkedBibItem">
        <span className="LinkedBibItem--headline">{headline} </span>
        <span className="LinkedBibItem--link">
          <a href={link} target="_blank">
            {linkLabel}
          </a>
        </span>
        <BibItem {...rest} />
      </div>
    );
  }
}
