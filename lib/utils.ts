export function parseAuthor(authors: string) {
  const fullNames = authors.split("and").map(value => {
    const parts: string[] = value
      .split(" ")
      .map(
        (k: string): string => {
          return k.trim();
        }
      )
      .filter(value => {
        return value.length > 0;
      });

    return parts;
  });

  const formattedFullNames = fullNames.map(fullName => {
    const out: any[] = [];

    if (fullName.length === 3) {
      out.push(fullName[0][0] + ".");
      out.push(fullName[1][0] + ".");
      out.push(fullName[2]);
    } else if (fullName.length === 2) {
      out.push(fullName[0][0] + ".");
      out.push(fullName[1]);
    } else {
      throw new Error(`invalid name: ${fullName}`);
    }

    return out.join(" ");
  });

  const joinedNames = formattedFullNames.join(", "),
    lastComma = joinedNames.lastIndexOf(",");

  if (lastComma >= 0) {
    const start = joinedNames.slice(0, lastComma);
    const end = joinedNames.slice(lastComma + 2, joinedNames.length);
    return `${start} and ${end}`;
  } else {
    return joinedNames;
  }
}

const MONTHS: string[] = [
  "Jan.",
  "Feb.",
  "Mar.",
  "Apr.",
  "May",
  "Jun.",
  "Jul.",
  "Aug.",
  "Sep.",
  "Oct.",
  "Nov.",
  "Dec."
];

export function parseMonth(month: string) {
  const m = parseInt(month, 10);
  if (isNaN(m)) {
    return `${month[0].toLocaleUpperCase()}${month.slice(1, 3)}.`;
  } else {
    if (m < 1 || m > 12) {
      throw new Error(`invalid month: ${m} is out of range`);
    }
    return MONTHS[m - 1];
  }
}
