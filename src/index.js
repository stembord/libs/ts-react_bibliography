"use strict";
exports.__esModule = true;
var LOOKUP_TABLE = {
    M: 1000,
    CM: 900,
    D: 500,
    CD: 400,
    C: 100,
    XC: 90,
    L: 50,
    XL: 40,
    X: 10,
    IX: 9,
    V: 5,
    IV: 4,
    I: 1
};
var SEARCH_KEYS_ORDERED = [
    "M",
    "CM",
    "D",
    "CD",
    "C",
    "XC",
    "L",
    "XL",
    "X",
    "IX",
    "V",
    "IV",
    "I"
];
function toRomanNumber(x) {
    var romanNumber = "";
    for (var index in SEARCH_KEYS_ORDERED) {
        if (SEARCH_KEYS_ORDERED.hasOwnProperty(index)) {
            var romanNumberSymbolKey = SEARCH_KEYS_ORDERED[index];
            while (x >= LOOKUP_TABLE[romanNumberSymbolKey]) {
                romanNumber += romanNumberSymbolKey;
                x -= LOOKUP_TABLE[romanNumberSymbolKey];
            }
        }
    }
    return romanNumber;
}
exports.toRomanNumber = toRomanNumber;
