import * as test from "tape";
import { parseAuthor, parseMonth } from "../lib/utils";

test("parseAuthor", (assert: test.Test) => {
  assert.equal(
    parseAuthor("John Faucett"),
    "J. Faucett",
    "shortens first name"
  );
  assert.equal(
    parseAuthor("John Seamus Faucett"),
    "J. S. Faucett",
    "shortens first and middle names"
  );
  assert.equal(
    parseAuthor("Johan Faucett and Nathan Faucett"),
    "J. Faucett and N. Faucett",
    "shortens first names and joins by and for 2 names"
  );

  assert.equal(
    parseAuthor("John Faucett and Olga Faucett and Nathan Faucett"),
    "J. Faucett, O. Faucett and N. Faucett",
    "shortens first names and joins by comma for 3 names"
  );

  assert.end();
});

test("parseMonth", (assert: test.Test) => {
  assert.equal(parseMonth("jan"), "Jan.", "jan => Jan.");
  assert.equal(parseMonth("1"), "Jan.", "1 => Jan.");
  assert.equal(parseMonth("mar"), "Mar.", "mar => Mar.");
  assert.equal(parseMonth("3"), "Mar.", "3 => Mar.");
  assert.equal(parseMonth("dec"), "Dec.", "dec => Dec.");
  assert.equal(parseMonth("12"), "Dec.", "12 => Dec.");

  assert.end();
});
