import { default as React } from "react";
import { render } from "react-dom";
import { BibItem, LinkedBibItem } from "../../lib";

render(
  <div>
    <ol>
      <li>
        <BibItem
          type="article"
          author="Peter Adams"
          title="The title of the work"
          journal="The name of the journal"
          year="1993"
          number="2"
          pages="201-213"
          month="7"
          note="An optional note"
          volume="4"
          doi="10.1007/978-3-540-28650-9_4"
          url="https://doi.org/10.1007/978-3-540-28650-9_4"
        />
      </li>
      <li>
        <LinkedBibItem
          headline="An interesting masterpiece"
          link="https://google.com"
          linkLabel="PDF"
          type="article"
          author="Peter Adams and Quincy Jones"
          title="The title of the work"
          journal="The name of the journal"
          year="1993"
          doi="10.1007/978-3-540-28650-9_4"
          url="https://doi.org/10.1007/978-3-540-28650-9_4"
        />
      </li>
    </ol>
  </div>,
  document.getElementById("app")
);
