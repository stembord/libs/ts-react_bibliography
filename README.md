# ts-react_bibliography

Reference implementation pages.
It uses the `ieeetr` format.

1. https://verbosus.com/bibtex-style-examples.html?lang=en

```json
"dependencies": {
  "@stembord/ts-react_bibliography": "git://gitlab.com/stembord/libs/ts-react_bibliography.git"
}
```

```tsx
import BibItem from "@stembord/ts-react_bibliography";

<BibItem
  type="article"
  author="Peter Adams"
  title="The title of the work"
  journal="The name of the journal"
  year={1993}
/>;
```
